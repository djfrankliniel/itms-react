import logo from './logo.svg';
import './App.css';
import './assets/output.css'

function App() {
  return (
    <div className="App">
      <p>Hello world!</p>
    </div>
  );
}

export default App;
